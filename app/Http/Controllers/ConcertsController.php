<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Genre;
use App\User;
use App\Calendar;


use Illuminate\Http\Request;

class ConcertsController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth'); 
    }


    public function index() 
    {
        $concerts = Concert::latest('id')->paginate(8);

    	return view('concerts.index', compact('concerts'));
    }

    public function concertsCalendar() 
    {

        $events = [];

        $concerts = Concert::latest('id')->paginate(8);

        foreach ($concerts as $concert) {
            $events[] = \Calendar::event(
                $concert->title, //event title
                true, //full day event?
                $concert->start_datetime, 
                $concert->start_datetime, 
                1, //optional event ID
                [
                    'url' => '/admin/concerts/'.$concert->id
                ]
            );
        }

        $calendar = \Calendar::addEvents($events)
                            ->setOptions([
                                'firstDay' => 1
                            ])->setCallbacks([
                                //somthing;
                            ]);

        return view('concerts.calendar', compact('concerts', 'calendar'));
    }

    public function show($id) 
    {

    	$concert = Concert::find($id);

    	if(is_null($concert)) {
    		abort(404);
    	}
    	
    	return view('concerts.show', compact('concert'));
    }

    public function create() 

    {
    	$genres = Genre::latest('id')->pluck('title', 'id');
    	$users = User::latest('id')->pluck('name', 'id');
    	return view('concerts.create', compact('genres', 'users'));
    }


    public function store(Request $request)  
    {

        $concert = new Concert($request->all());

        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'city' => 'required|min:3',
            'location' => 'required|min:3',
            'ticket_price' => 'numeric|min:0',
            'ticket_quantity' => 'numeric|min:0',
        ]);

        $concert->save();

        $genres = $request->input('genres');
        $concert->genres()->attach($genres);

        return redirect('admin/concerts_calendar');
    
    }

    public function edit($id) 
    {
        $concert = Concert::select()->where('id', $id)->first();
        $genres = Genre::latest('id')->pluck('title', 'id');
        $users = User::latest('id')->pluck('name', 'id');

        return view('concerts.edit', compact('concert', 'genres', 'users'));
    }

    public function update($id, Request $request) 
    {

        $concert = Concert::select()->where('id', $id)->first();

        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'city' => 'required|min:3',
            'location' => 'required|min:3',
            'ticket_price' => 'numeric|min:0',
            'ticket_quantity' => 'numeric|min:0',
        ]);

        $genres = $request->input('genres');
        $concert->genres()->sync($genres);

        $concert->update($request->all());



        return redirect('admin/concerts_calendar');
    }

    public function delete($id) 
    {
        $concert = Concert::find($id);
        $concert->delete();
        return redirect('admin/concerts_calendar');
    }

}
