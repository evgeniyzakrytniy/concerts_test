<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concert extends Model
{
    protected $fillable = [
        'title', 
        'city',
		'location',
		'start_datetime',
		'ticket_price',
		'ticket_quantity',
		'is_our_company',
		'genre_id',
		'user_id',
    ];


    public function genres() 
    {
        return $this->belongsToMany('App\Genre')->withTimestamps();
    }

    public function user() 
    {
        return $this->belongsTo('App\User');
    }

}
