@extends('home')

@section('content')
	
		

	<h3>Жанры</h3>
	<a class="btn btn-success" href="{{ url('/admin/genres/create') }}">Добавить жанр</a>
		
	<br><br>
	

	<div class="row">

	<table class="table">
		<thead>
			<th>id</th>
			<th>Название</th>
			<th>Управление</th>
		</thead>
		<tbody>
			@foreach ($genres as $genre)
				<tr>
					<td>{{ $genre->id }}</td>
					<td>{{ $genre->title }}</td>
					<td>
						<a class="btn-primary btn-sm" href="/admin/genres/{{ $genre->id }}/edit">Изменить</a> 
						<a class="btn-danger btn-sm" href="/admin/genres/{{ $genre->id }}/delete">Удалить</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>	

			
		</div>

@stop