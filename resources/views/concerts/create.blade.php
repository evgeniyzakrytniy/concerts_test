@extends('home')

@section('content')
		<div class="container">
			<h1>Добавить концерт</h1>
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<hr>
						{!! Form::open(['url' => 'admin/concerts']) !!}
							
							
							@include('concerts.form', ['submitButtonText' => 'Добавить концерт'])

							
						{!! Form::close() !!}

	</div>
@stop