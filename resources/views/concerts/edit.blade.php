@extends('home')

@section('content')
	<h1>Редактирование: {!! $concert->title !!}</h1>
	@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
	@endif
	<hr>
						{!! Form::model($concert, ['method' => 'PATCH', 'action' => ['ConcertsController@update', $concert->id]]) !!}
							
							@include('concerts.form', ['submitButtonText' => 'Изменить категорию'])

						{!! Form::close() !!}

						

@stop