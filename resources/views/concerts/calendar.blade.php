
@extends('home')

	<link href="{{ asset('fullcalendar-3.9.0/fullcalendar.min.css') }}" rel="stylesheet">
	<link href="{{ asset('fullcalendar-3.9.0/fullcalendar.print.min.css') }}" rel="stylesheet" media='print'>
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ asset('fullcalendar-3.9.0/lib/jquery.min.js') }}"></script>
  
    <script type="text/javascript" src="{{ asset('fullcalendar-3.9.0/lib/moment.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('fullcalendar-3.9.0/fullcalendar.min.js') }}"></script>

@section('content')

		<h3>Концерты</h3>
		<a class="btn btn-success" href="{{ url('/admin/concerts/create') }}">Добавить концерт</a>
		<br><br>

		{!! $calendar->calendar() !!}
		{!! $calendar->script() !!}

@stop

<style type="text/css">
	.fc-toolbar .fc-right, .fc-today-button {
		display: none;
	}
</style>