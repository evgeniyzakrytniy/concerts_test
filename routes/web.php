<?php


use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use Collective\Html\FormFacade;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin/concerts_calendar');
});

Auth::routes();

Route::get('/home', function () {
    return redirect('admin/concerts_calendar');
});

Route::resource('/admin/concerts', 'ConcertsController'); 
Route::get('admin/concerts_calendar', 'ConcertsController@concertsCalendar');
Route::get('admin/concerts/{id}/delete', 'ConcertsController@delete');

Route::resource('/admin/genres', 'GenresController'); 
Route::get('admin/genres/{id}/delete', 'GenresController@delete');
