@extends('home')

@section('content')
	<h1>Редактирование: {!! $genre->title !!}</h1>
	@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
	@endif
	<hr>
						{!! Form::model($genre, ['method' => 'PATCH', 'action' => ['GenresController@update', $genre->id]]) !!}
							
							@include('genres.form', ['submitButtonText' => 'Изменить жанр'])

						{!! Form::close() !!}

						

@stop