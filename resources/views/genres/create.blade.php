@extends('home')

@section('content')
		<div class="container">
			<h1>Добавить жанр</h1>
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<hr>
						{!! Form::open(['url' => 'admin/genres']) !!}
							
							
							@include('genres.form', ['submitButtonText' => 'Добавить жанр'])

							
						{!! Form::close() !!}

	</div>
@stop