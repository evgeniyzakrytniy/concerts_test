@extends('home')

@section('content')
	
		

	<h3>{{ $concert->title }} ({{ $concert->city }}, {{ $concert->location }})</h3>
	<br><br>
	<a class="btn btn-success" href="{{ url('/admin/concerts/create') }}">Добавить концерт</a>
	<a class="btn-primary btn-sm" href="/admin/concerts/{{ $concert->id }}/edit">Изменить</a> 
	<a class="btn-danger btn-sm" href="/admin/concerts/{{ $concert->id }}/delete">Удалить</a>
	<br><br>
	

	<div class="row">

	<table class="table">

		<tbody>
				

				<tr>
					<td>id</td>
					<td>{{ $concert->id }}</td>
				</tr>
				<tr>
					<td>Название</td>
					<td>{{ $concert->title }}</td>
				</tr>
				<tr>
					<td>Город</td>
					<td>{{ $concert->city }}</td>
				</tr>
				<tr>
					<td>Место проведения</td>
					<td>{{ $concert->location }}</td>
				</tr>
				<tr>
					<td>Дата и время начала</td>
					<td>{{ $concert->start_datetime }}</td>
				</tr>
				<tr>
					<td>Цена билета</td>
					<td>{{ $concert->ticket_price }}</td>
				</tr>
				<tr>
					<td>Кол-во билетов</td>
					<td>{{ $concert->ticket_quantity }}</td>
				</tr>
				<tr>
					<td>Кто организатор?</td>
					<td>
						@if ($concert->is_our_company == 1)
							Наша компания
						@else
							Конкуренты
						@endif
					</td>
				</tr>
				<tr>
					<td>Жанры</td>
					<td>
						@foreach ($concert->genres as $genre) 
							<a class="tag" href="/admin/genres">{{ $genre->title }}</a><br> 
						@endforeach
					</td>
				</tr>
				<tr>
					<td>Администратор</td>
					<td>{{ $concert->user->name }}</td>
				</tr>

		</tbody>
	</table>	

			
		</div>

@stop