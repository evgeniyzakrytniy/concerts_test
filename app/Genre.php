<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
	
    protected $fillable = [
        'title', 
    ];


    public function concerts() {
    	return $this->belongsToMany('App\Concert')->withTimestamps();
    }

}
