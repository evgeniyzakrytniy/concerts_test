							

							<div class="form-group">

								{!! Form::label('title', 'Название:') !!}
								{!! Form::text('title', null, ['class' => 'form-control']) !!}

							</div>		

							<div class="form-group">
							
								{!! Form::label('city', 'Город:') !!}
								{!! Form::text('city', null, ['class' => 'form-control']) !!}
							
							</div>

							<div class="form-group">
							
								{!! Form::label('location', 'Место проведения:') !!}
								{!! Form::text('location', null, ['class' => 'form-control']) !!}
							
							</div>

							<div class="form-group" >

				                @if(isset($concert->start_datetime))
				              
				                  {!! Form::label('start_datetime', 'Дата публикации:') !!}
				                  {!! Form::input('datetime', 'start_datetime', date('Y-m-d H:i:s', strtotime($concert->start_datetime)), ['class' => 'form-control']) !!}

				                @else 

				                  {!! Form::label('start_datetime', 'Дата публикации:') !!}
				                  {!! Form::input('datetime', 'start_datetime', date('Y-m-d H:i:s'), ['class' => 'form-control']) !!}
				                
				                @endif

				            </div>

				            <div class="form-group">
							
								{!! Form::label('ticket_price', 'Цена билета:') !!}
								{!! Form::text('ticket_price', null, ['class' => 'form-control']) !!}
							
							</div>

							<div class="form-group">
							
								{!! Form::label('ticket_quantity', 'Кол-во билетов:') !!}
								{!! Form::text('ticket_quantity', null, ['class' => 'form-control']) !!}
							
							</div>

							<div class="form-group">

	                            <label for="is_our_company" class="col-md-4 control-label">Кто организатор:</label>

	                            <select id="is_our_company" name="is_our_company" type="is_our_company" class="form-control" required>
	                                <option value="1">Наша компания</option>
	                                <option value="0">Конкуренты</option>
	                            </select>

	                            
                        	</div>

							<div class="form-group">

			                	{!! Form::label('genres', 'Жанры:') !!}
			                	{!! Form::select('genres[]', $genres, null, ['class' => 'form-control form-genres-blck', 'multiple']) !!}
			              
			              	</div>

							<div class="form-group">
							
								{!! Form::label('user_id', 'Администратор:') !!}
			                	{!! Form::select('user_id', $users, null, ['class' => 'form-control form-users-blck']) !!}
							
							</div>

							<div class="form-group">
							
								{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
							
							</div>