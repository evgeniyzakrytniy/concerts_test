@extends('home')

@section('content')
	
		

	<h3>Концерты</h3>
	<a class="btn btn-success" href="{{ url('/admin/concerts/create') }}">Добавить концерт</a>
		
	<br><br>
	

	<div class="row">

	<table class="table">
		<thead>
			<th>id</th>
			<th>Название</th>
			<th>Город</th>
			<th>Место проведения</th>
			<th>Дата и время начала</th>
			<th>Цена билета</th>
			<th>Кол-во билетов</th>
			<th>Кто организатор?</th>
			<th>Жанры</th>
			<th>Администратор</th>
		</thead>
		<tbody>
			@foreach ($concerts as $concert)
				<tr>
					<td>{{ $concert->id }}</td>
					<td>{{ $concert->title }}</td>
					<td>{{ $concert->city }}</td>
					<td>{{ $concert->location }}</td>
					<td>{{ $concert->start_datetime }}</td>
					<td>{{ $concert->ticket_price }}</td>
					<td>{{ $concert->ticket_quantity }}</td>
					<td>
						@if ($concert->is_our_company == 1)
							Наша компания
						@else
							Конкуренты
						@endif
					</td>
					<td>
						@foreach ($concert->genres as $genre) 
							<a class="tag" href="/admin/genres">{{ $genre->title }}</a><br> 
						@endforeach
					</td>
					<td>{{ $concert->user_id }}</td>
					<td>
						<a class="btn-primary btn-sm" href="/admin/concerts/{{ $concert->id }}/edit">Изменить</a> 
						<a class="btn-danger btn-sm" href="/admin/concerts/{{ $concert->id }}/delete">Удалить</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>	

			
		</div>

@stop