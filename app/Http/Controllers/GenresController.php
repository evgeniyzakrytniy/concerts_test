<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Genre;

use Illuminate\Http\Request;

class GenresController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth'); 
    }


    public function index() 
    {
        $genres = Genre::latest('id')->paginate(8);

    	return view('genres.index', compact('genres'));
    }

    public function show($alias) 
    {
        //dd($alias);
    	$genre = Genre::select()->where('alias', $alias)->first();

    	if(is_null($genre)) {
    		abort(404);
    	}
    	
    	return view('genres.index', compact('genre'));
    }

    public function create() 
    {
    	return view('genres.create');
    }


    public function store(Request $request)  
    {

        $category = new Genre($request->all());

        $validatedData = $request->validate([
            'title' => 'required|min:3',
        ]);

        $category->save();

        return redirect('admin/genres');
    
    }

    public function edit($id) 
    {
        $genre = Genre::select()->where('id', $id)->first();
        return view('genres.edit', compact('genre'));
    }

    public function update($id, Request $request) 
    {

        $genre = Genre::select()->where('id', $id)->first();

        $validatedData = $request->validate([
            'title' => 'required|min:3',
        ]);

        $genre->update($request->all());

        return redirect('admin/genres');
    }

    public function delete($id) 
    {
        $genre = Genre::find($id);
        $genre->delete();
        return redirect('admin/genres');
    }
}
